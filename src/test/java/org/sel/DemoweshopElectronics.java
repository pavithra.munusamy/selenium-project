package org.sel;

import java.awt.AWTException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoweshopElectronics {

public static void main(String[] args) throws AWTException, InterruptedException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://demowebshop.tricentis.com");
		
		driver.findElement(By.xpath("//a[@href='/login']")).click();
		
		driver.findElement(By.id("Email")).sendKeys("pavipragya@gmail.com");
		driver.findElement(By.name("Password")).sendKeys("pragya@123");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		Thread.sleep(3000);
		
		WebElement Electronics = driver.findElement(By.xpath("//ul[@class='top-menu']//a[@href='/electronics']"));
		Actions a=new Actions(driver);
		a.moveToElement(Electronics).perform();
		
		driver.findElement(By.linkText("Electronics")).click();
		driver.findElement(By.xpath("//img[@alt='Picture for category Cell phones']")).click();
		//Thread.sleep(2000);
		
		WebElement phone = driver.findElement(By.id("products-orderby"));
		Select sel=new Select(phone);
		sel.selectByVisibleText("Price: High to Low");
		
		
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		
		driver.findElement(By.linkText("Shopping cart")).click();
		
		driver.findElement(By.id("termsofservice")).click();
		
		driver.findElement(By.id("checkout")).click();
		
		driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
		//1button
		
		driver.findElement(By.xpath("//input[@onclick='Shipping.save()']")).click();
		//2button
		
		driver.findElement(By.xpath("//input[@id='shippingoption_0']")).click();
		driver.findElement(By.xpath("//input[@onclick='ShippingMethod.save()']")).click();
		//3button
		
		//driver.findElement(By.id("paymentmethod_2")).click();
		//driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
		//3button
		
		/*WebElement visa = driver.findElement(By.id("CreditCardType"));
		Select sel1= new Select(visa);
		sel1.deselectByIndex(0);
		
		driver.findElement(By.id("CardholderName")).sendKeys("pavithra");
		driver.findElement(By.id("CardNumber")).sendKeys("108469563927");
		
		WebElement date = driver.findElement(By.id("ExpireMonth"));
		Select sel2=new Select(date);
		sel2.selectByIndex(5);
		
		WebElement year = driver.findElement(By.id("ExpireYear"));
		Select s=new Select(year);
		s.selectByIndex(1); */
		
		driver.findElement(By.xpath("//input[@onclick='PaymentMethod.save()']")).click();
		//driver.findElement(By.xpath("//input[@value='Payments.CashOnDelivery']")).click();
		//4button
		
		driver.findElement(By.xpath("//input[@onclick='PaymentInfo.save()']")).click();
		//5button
		
		driver.findElement(By.xpath("//input[@onclick='ConfirmOrder.save()']")).click();
		//6button
		
		driver.findElement(By.xpath("//input[@class='button-2 order-completed-continue-button']")).click();
		//7button
		
		WebElement str = driver.findElement(By.xpath("//div[@class='section order-completed']"));
		System.out.println(str.getText());
		
		driver.findElement(By.linkText("Log out")).click();
		
		
}
}
