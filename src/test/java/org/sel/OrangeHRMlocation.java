package org.sel;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class OrangeHRMlocation {
	public static void main(String[] args) throws AWTException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//a[@href='/web/index.php/admin/viewAdminModule']")).click();
		
		driver.findElement(By.xpath("//span[text()='Organization ']")).click();
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		
		driver.findElement(By.xpath("//a[text()='Locations']")).click();
		
		driver.findElement(By.xpath("//button[@class='oxd-button oxd-button--medium oxd-button--secondary']")).click();
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[1]")).sendKeys("Paul collings");
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[2]")).sendKeys("ohio");
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[3]")).sendKeys("Renoldsbug");
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[4]")).sendKeys("43068");
		
		driver.findElement(By.xpath("//i[@class='oxd-icon bi-caret-down-fill oxd-select-text--arrow']")).click();
		driver.findElement(By.xpath("//*[contains(text(),'United States')]")).click();
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[5]")).sendKeys("9573956289");
		
		driver.findElement(By.xpath("(//input[@placeholder='Type here ...'])[6]")).sendKeys("4973967");
		
		driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[1]"))
		.sendKeys("2793 Taylor Rd Ext, Reynoldsburg, OH 43068, United States");
driver.findElement(By.xpath("(//textarea[@placeholder='Type here ...'])[2]")).sendKeys("hello");
		
		
		
	
	}
		
	}
	
	
