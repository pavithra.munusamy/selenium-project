package org.data;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Fetchingdatathroughtapachepoixlsx {
	
	public static void main(String[] args) throws IOException {
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://demowebshop.tricentis.com");
		File f=new File("/home/pavithra/Documents/Test_data/Testdata2.xlsx");
		FileInputStream fis=new FileInputStream(f);
		XSSFWorkbook workbook= new XSSFWorkbook(fis);
		XSSFSheet sheet=workbook.getSheetAt(0);
		
		int rows= sheet.getPhysicalNumberOfRows();
		for (int i=1;i<rows;i++) {
			
			/*int columns=sheet.getRow(i).getLastCellNum();
			for (int j=0;j<columns;j++) {
				String cellvalue=sheet.getRow(i).getCell(j).getStringCellValue();
				System.out.println(cellvalue);
				
			}*/
			String username=sheet.getRow(i).getCell(0).getStringCellValue();
			String password=sheet.getRow(i).getCell(1).getStringCellValue();
			WebElement login = driver.findElement(By.linkText("Log in"));
			login.click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@value='Log in']")).click();
			driver.findElement(By.linkText("Log out")).click();
		}
		
	}

}
