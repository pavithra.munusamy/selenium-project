package org.sel;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class OrangeHRM {
	
	public static void main(String[] args) throws AWTException, InterruptedException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://opensource-demo.orangehrmlive.com/web/index.php/auth/login");
		
		
		driver.findElement(By.name("username")).sendKeys("Admin");
		driver.findElement(By.name("password")).sendKeys("admin123");
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//a[@href='/web/index.php/admin/viewAdminModule']")).click();
		
		driver.findElement(By.xpath("//i[@class='oxd-icon bi-plus oxd-button-icon']")).click();
		
		
		driver.findElement(By.xpath("(//div[@tabindex='0'])[1]")).click();
		Robot r=new Robot();
		r.keyPress(KeyEvent.VK_DOWN);
		r.keyRelease(KeyEvent.VK_DOWN);
		driver.findElement(By.xpath("//div[text()='Admin']")).click();
		//r.keyPress(KeyEvent.VK_ENTER);
		//9r.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(2000);
		
		
		driver.findElement(By.xpath("//input[@placeholder='Type for hints...']")).sendKeys("Paul collings");
		
		driver.findElement(By.xpath("(//div[@class='oxd-select-text oxd-select-text--active'])[2]")).click();
		Robot r1=new Robot();
		r1.keyPress(KeyEvent.VK_DOWN);r.keyRelease(KeyEvent.VK_DOWN);
		driver.findElement(By.xpath("//div[text()='Enabled']")).click();
		
		driver.findElement(By.xpath("(//input[@autocomplete='off'])[3]")).sendKeys("Paul collings");
		
		driver.findElement(By.xpath("(//input[@type='password'])[1]")).sendKeys("Admin@123");
		
		driver.findElement(By.xpath("(//input[@type='password'])[2]")).sendKeys("Admin@123");
	}

}
