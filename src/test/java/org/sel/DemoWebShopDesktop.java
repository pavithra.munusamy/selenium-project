package org.sel;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class DemoWebShopDesktop {
	
public static void main(String[] args) throws AWTException, InterruptedException {
		
		WebDriver driver=new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
		driver.get("https://demowebshop.tricentis.com");
		
		driver.findElement(By.xpath("//a[@href='/login']")).click();
		
		driver.findElement(By.id("Email")).sendKeys("pavipragya@gmail.com");
		driver.findElement(By.name("Password")).sendKeys("pragya@123");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();
		Thread.sleep(3000);
		
		
		WebElement computers = driver.findElement(By.xpath("//ul[@class='top-menu']//a[@href='/computers']"));
		Actions a=new Actions(driver);
		a.moveToElement(computers).perform();
		
		
		driver.findElement(By.xpath("(//a[@href='/desktops'])[1]")).click();
		//Robot r=new Robot();
		//r.keyPress(KeyEvent.VK_DOWN);
		//r.keyPress(KeyEvent.VK_ENTER);
		
		WebElement ele = driver.findElement(By.id("products-orderby"));
		Select s=new Select(ele);
		s.selectByVisibleText("Price: Low to High");
	//	s.selectByIndex(3);
		
		driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("(//input[@type='button'])[2]")).click();
				
		driver.findElement(By.xpath("//span[text()='Shopping cart']")).click();
		
		driver.findElement(By.xpath("//div[@class='terms-of-service']//input[@type='checkbox']")).click();
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		
		driver.findElement(By.xpath("//option[text()='pavithra pavithra, asgdhfj, qedfghjk, Alaska 9867564, United States']")).click();
		
		
		/*driver.findElement(By.xpath("//label[text()='First name:']")).sendKeys("pavithra");
		driver.findElement(By.xpath("//label[text()='Last name:']")).sendKeys("pavithra");
		driver.findElement(By.xpath("//label[text()='Email:']")).sendKeys("pavipragya@gmail.com");
		driver.findElement(By.xpath("//label[text()='Company:']")).sendKeys("GL");
		WebElement country = driver.findElement(By.xpath("//label[text()='Country:']"));
		Select s1=new Select(country);
		s1.deselectByVisibleText("United States");
		driver.findElement(By.xpath("//label[text()='City:']")).sendKeys("London");
		driver.findElement(By.xpath("//label[text()='Address 1:']")).sendKeys("London,paris");
		driver.findElement(By.xpath("//label[text()='Address 2:']")).sendKeys("paris");
		driver.findElement(By.xpath("//label[text()='Zip / postal code:']")).sendKeys("584639");
		driver.findElement(By.xpath("//label[text()='Phone number:']")).sendKeys("9876467979");
		driver.findElement(By.xpath("//label[text()='Fax number:']")).sendKeys("98757885"); */
		
		driver.findElement(By.xpath("//div[@Id='billing-buttons-container']//input[@type='button']")).click();
		Thread.sleep(2000);
		driver.findElement(By.xpath("(//input[@class='button-1 new-address-next-step-button'])[2]")).click();
		//driver.findElement(By.xpath("//input[@onclick='Billing.save()']")).click();
		driver.findElement(By.xpath("//span[@id='shipping-please-wait']")).click();
		//driver.findElement(By.xpath("//span[text()=' Loading next step...']")).click();
		driver.findElement(By.xpath("//label[text()='Ground (0.00)']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 shipping-method-next-step-button']")).click();
		driver.findElement(By.xpath("//label[text()='Cash On Delivery (COD) (7.00)']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 payment-method-next-step-button']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 payment-info-next-step-button']")).click();
		driver.findElement(By.xpath("//input[@class='button-1 confirm-order-next-step-button']")).click();
		
		driver.findElement(By.xpath("//input[@class='button-2 order-completed-continue-button']")).click();
		driver.findElement(By.linkText("Log out")).click();
		System.out.println("Order number: 1359872");
		
		
}
}